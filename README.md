# Fastapi Tutorial

[FastAPI](https://fastapi.tiangolo.com) is a Python framework for building APIs

## Installation

project setup

```bash
pip install --no-cache-dir --upgrade -r ./requirements.txt
```
or
```bash
conda install --file=requirements.txt
```

## Usage

```bash
uvicorn tutorial:app --reload
```

## License

[MIT](https://opensource.org/licenses/MIT/)