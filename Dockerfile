FROM python:3.11

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r ./requirements.txt

COPY ./ /code

RUN ["cp","env",".env"]

RUN ["python","sqlite.py"]

CMD ["uvicorn", "tutorial:app", "--host", "0.0.0.0", "--port", "80"]