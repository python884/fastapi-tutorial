import sqlite3
import os
from dotenv import load_dotenv

load_dotenv()

class User:
    @staticmethod
    def get_all():
        con = sqlite3.connect(os.getenv("DB_FILE"))
        cur = con.cursor()
        sql = """
            select * from user order by name asc
            """
        result = cur.execute(sql)
        userlist = result.fetchall()
        return userlist

    @staticmethod
    def get_user_info(user_id: str):
        con = sqlite3.connect(os.getenv("DB_FILE"))
        cur = con.cursor()
        sql = """
            select * from user where id=?
            """
        result = cur.execute(sql, user_id)
        user_info = result.fetchone()
        return user_info

    @staticmethod
    def add_new_user(name: str, email: str):
        con = sqlite3.connect(os.getenv("DB_FILE"))
        cur = con.cursor()
        sql = """
            insert into user(name,email) values(?,?);
        """
        cur.execute(sql, (name, email))
        con.commit()
        return cur.lastrowid

    @staticmethod
    def update_user_info(id: str, name: str, email: str):
        con = sqlite3.connect(os.getenv("DB_FILE"))
        cur = con.cursor()
        sql = """
            update user set name=?, email=? where id=?;
        """
        cur.execute(sql, (name, email, id))
        con.commit()

    @staticmethod
    def delete_user_info(id: str):
        con = sqlite3.connect(os.getenv("DB_FILE"))
        cur = con.cursor()
        sql = """
            delete from user where id=?;
        """
        cur.execute(sql, (id))
        con.commit()
