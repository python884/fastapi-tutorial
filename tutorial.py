from fastapi import FastAPI, Path, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field, EmailStr
from model.User import User as UserDB

app = FastAPI(
    title="User RESTful API",
    description="Basic User RESTful API built with fastapi",
    version="0.0.1",
    license_info={
        "name": "MIT",
        "url": "https://opensource.org/licenses/MIT",
    },
    redoc_url=None
)
origins = [
    "http://localhost",
    "http://127.0.0.1",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class User(BaseModel):
    name: str
    email: EmailStr = Field(default="", title="User email")


class UserInDB(User):
    id: int


@app.get("/")
def index():
    """
    connection testing
    """
    return {"Simple User RESTAPI built with FastAPI and SQLite"}


@app.get("/users")
def get_all_users():
    """
    get all users information
    """
    
    userlist = UserDB.get_all()
    data = []
    for u in userlist:
        data.append(UserInDB(id=u[0], name=u[1], email=u[2]))
    return data


@app.get("/user/{user_id}")
def get_user_info(user_id: int, q: str | None = None):
    """
    get user info by ID
    """
    user = UserDB.get_user_info(str(user_id))
    if user is None:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, content={"detail": "User not found"}
        )
    else:
        user = UserInDB(id=user[0], name=user[1], email=user[2])
        return user

@app.post("/user")
def add_user_info(user: User):
    """
    add new user
    """
    new_id = UserDB.add_new_user(*user)
    return {"result": f"user created with ID: {new_id}"}


@app.patch("/user/{user_id}")
def update_user_info(
    *,
    user_id: int = Path(title="user id", description="user unique id"),
    user_info: User,
):
    """
    update user information
    """
    UserDB.update_user_info(str(user_id),user_info.name,user_info.email)
    return {"result": f"user information updated"}

@app.delete("/user/{user_id}")
def delete_user_info(user_id: int = Path(title="user id", description="user unique identifier")):
    """
    remove user information
    """
    UserDB.delete_user_info(str(user_id))
    return {"result": f"user information deleted"}
