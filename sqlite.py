import sqlite3

# create database file if not found
db_file = "tutorial.db"
file = open(db_file, 'w+')
file.close()

# insert some data into the sqlite database
con = sqlite3.connect(db_file)

cursor = con.cursor()

cursor.execute('''
create table if not exists user(
        id integer not null primary key AUTOINCREMENT,
        name varchar(50) not null,
        email varchar(50) not null
    )
''')
cursor.execute('''
insert into user(name,email) values('alex','alex@email.com'),('john','john@email.com')
''')
con.commit()
print("database setup")
